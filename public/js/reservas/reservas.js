jQuery(document).ready(function()
{

  jQuery('.container').on('click', '.reservar', function()
  {

    $(this).data("reservado","prereserva");
    $(this).addClass("quitar");
    $(this).removeClass("reservar");
    $(this).css({'background':'#888888'});

  });

  jQuery('.container').on('click', '.quitar', function()
  {

    $(this).data("reservado","false");
    $(this).addClass("reservar");
    $(this).removeClass("quitar");
    $(this).css({'background':'#FFFFFF'});

  });

  jQuery('.container').on('click', '.reservarbtn', function(e)
  {

    e.preventDefault();
    var silla = new Array();

    $(".quitar").each(function(index) {
      silla[index] = $(this).data('codreserva');
    });

    var fecha_reserva = $("#fecha_reserva").val();
    var nombre        = $("#nombre").val();
    var apellidos     = $("#apellidos").val();

    $.ajax({
      type : 'POST',
      url  : '/reservar',
      data : {fecha_reserva:fecha_reserva, apellidos:apellidos, nombre:nombre, sillas:silla},

      success:function(data){
        alert("Reserva Guardada Correctamente");

        $("#fecha_reserva").val("");
        $("#nombre").val("");
        $("#apellidos").val("");
        $(".quitar").css({'background':'#000000'});
        $(".quitar").removeClass("quitar");

      }
    });

  });

  jQuery('.container').on('change', '#fecha_reserva', function()
  {

    var fecha_reserva = $(this).val();

    $.ajax({
      type : 'GET',
      url  : '/',
      data : {fecha_reserva:fecha_reserva},
      success:function(data){
        $("#calendario_resera").html(data);
      }
    });

  });

});
