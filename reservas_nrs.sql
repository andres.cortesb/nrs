-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-06-2018 a las 07:10:33
-- Versión del servidor: 5.7.14
-- Versión de PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reservas_nrs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nrs_capacidad`
--

CREATE TABLE `nrs_capacidad` (
  `id_capacidad` int(11) NOT NULL,
  `filas` int(11) NOT NULL,
  `columnas` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nrs_capacidad`
--

INSERT INTO `nrs_capacidad` (`id_capacidad`, `filas`, `columnas`) VALUES
(1, 5, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nrs_reservas`
--

CREATE TABLE `nrs_reservas` (
  `id_reserva` int(11) NOT NULL,
  `nombre` varchar(256) NOT NULL,
  `apellido` varchar(256) NOT NULL,
  `fecha_reserva` date NOT NULL,
  `cantidad_personas` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nrs_reservas`
--

INSERT INTO `nrs_reservas` (`id_reserva`, `nombre`, `apellido`, `fecha_reserva`, `cantidad_personas`) VALUES
(1, 'andres', 'cortes', '2018-06-18', 0),
(2, 'andres', 'cortes', '2018-06-18', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nrs_reserva_sillas`
--

CREATE TABLE `nrs_reserva_sillas` (
  `id_reserva` int(11) NOT NULL,
  `fila` int(11) NOT NULL,
  `columna` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nrs_reserva_sillas`
--

INSERT INTO `nrs_reserva_sillas` (`id_reserva`, `fila`, `columna`) VALUES
(1, 2, 3),
(1, 3, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `nrs_capacidad`
--
ALTER TABLE `nrs_capacidad`
  ADD PRIMARY KEY (`id_capacidad`);

--
-- Indices de la tabla `nrs_reservas`
--
ALTER TABLE `nrs_reservas`
  ADD PRIMARY KEY (`id_reserva`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `nrs_capacidad`
--
ALTER TABLE `nrs_capacidad`
  MODIFY `id_capacidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `nrs_reservas`
--
ALTER TABLE `nrs_reservas`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
