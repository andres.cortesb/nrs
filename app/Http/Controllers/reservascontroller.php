<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class ReservasController extends Controller{

  public function index(){

    $fecha_reserva = null;
    $input = request()->all();

    if(isset($input['fecha_reserva'])){
      $fecha_reserva = $input['fecha_reserva'];
    }

    $aforo  = DB::table('_capacidad')->first();
    $resdes = $this->getDisponibilidad($fecha_reserva);

    if(is_null($fecha_reserva)){
      return view('reservas', ['aforo' => $aforo, 'disponibilidad' => $resdes]);
    }else{
      return view('sillas', ['aforo' => $aforo, 'disponibilidad' => $resdes]);
    }

  }

  public function savereserva(){

    $input = request()->all();

    $input['cantidad'] = count($input['sillas']);

    $id_reserva = DB::table('_reservas')->insertGetId(
      ['apellido' => $input['apellidos'], 'nombre' => $input['nombre'], 'cantidad_personas' => $input['cantidad'], 'fecha_reserva' => $input['fecha_reserva']]
    );

    for($i = 0; $i < $input['cantidad']; $i++){
      $espacio = str_split($input['sillas'][$i]);
      DB::table('_reserva_sillas')->insertGetId(['id_reserva' => $id_reserva, 'fila' => $espacio[0], 'columna' => $espacio[1]]);
    }

    $this->saveFile($input);

    return "ok";

  }

  public function getDisponibilidad($fecha_reserva){

    $resdes = array();

    IF(is_null($fecha_reserva)){
        $fecha_reserva = date('Y-m-d');
    }

    $disponibilidad = DB::table('_reserva_sillas')
      ->select(DB::raw("CONCAT(fila,columna) as reserva"))
      ->Join('_reservas', '_reserva_sillas.id_reserva', '=', '_reservas.id_reserva')
      ->where('_reservas.fecha_reserva', $fecha_reserva)->get();

    foreach ($disponibilidad as $key => $disponibilidads) {
      $resdes[$key] = $disponibilidads->reserva;
    }

    return $resdes;

  }

  public function saveFile($data){

    $nombre = "";
    if (!empty($data['nombre'])){
      $nombre = $data['nombre'];
    }

    $apellidos  = "";
    if (!empty($data['apellidos'])){
      $apellidos = $data['apellidos'];
    }

    $cantidad = "";
    if (!empty($data['cantidad'])){
      $cantidad = $data['cantidad'];
    }

    $fecha_reserva = "";
    if (!empty($data['fecha_reserva'])){
      $fecha_reserva = $data['fecha_reserva'];
    }

    //Luego sobrescribo el txt

    $archivo  = "datos.txt";
    $file     = fopen($archivo, "a");
    fwrite($file, "Nombre: ".$nombre." Apellidos: ".$apellidos." Cantidad: ".$cantidad." Fecha reserva: ".$fecha_reserva."/n");
    fclose($file);

  }

}
