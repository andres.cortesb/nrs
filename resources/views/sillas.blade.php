

  @for($i = 0; $i < $aforo->filas; $i++)

    <?php $f = $i+1; ?>

    <div class="col-md text-center">Fila ({{$f}})</div>

    <div class="col-md-11 mb-3">
      <div class="row">

        @for($n = 0; $n < $aforo->columnas; $n++)

          <?php $s    = $n+1;
          $silla      = $f.$s;
          $color      = "";
          $reservado  = "false";
          $reservar   = "reservar";

          if(!empty($disponibilidad)){
            if(in_array($silla, $disponibilidad)){
              $color      = 'background: #000';
              $reservado  = 'true';
              $reservar   = "";
            }
          } ?>

          <div class="col {{$reservar}}" style="{{$color}}" data-codreserva=" {{$f}}{{$s}}" data-reservado="{{$reservado}}">
            <img src="images/silla.png" style="width: 60%;"  class="img-fluid rounded mx-auto d-block" alt="Responsive image">
            <div class="text-center"> ({{$f}} {{$s}}) </div>
          </div>
        @endfor

      </div>
    </div>

  @endfor
