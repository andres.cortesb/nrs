<!doctype html>

<html lang="{{ app()->getLocale() }}">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="css/product.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <script src="js/jquery.min.js" ></script>
    <script src="js/bootstrap.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="js/reservas/reservas.js" ></script>

    <title>Prueba Reserva NRS</title>

        <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

  </head>

  <script>

    // CSRF for all ajax call
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });

  </script>

  <body>

    <div class="container">

      <!-- Content here -->
      <div class="row justify-content-md-center mb-4">
        <h1>Prueba Reservas NRS</h1>
      </div>

      <form action="{{ url('/reservar') }}" method="POST">

        <div class="row">
          <div class="input-group">
            <div class="col-md-2 mb-3">
              <label for="fecha_reserva">Fecha de Reserva</label>
            </div>
            <div class="col-md-3 mb-3">
              <input  type="text" class="form-control datepicker" id="fecha_reserva" name="fecha_reserva" placeholder="YYYY-MM-DD" />
            </div>
            <script>
                $('#fecha_reserva').datepicker({
                    uiLibrary: 'bootstrap4',
                    format: "yyyy-mm-dd",
                });
            </script>
          </div>
        </div>

        <div class="row">
          <div class="input-group">
            <div class="col-md-2">
                <label for="nombre">Datos Comprador</label>
            </div>

            <div class="col-md-4">
              <input type="text" class="form-control mb-3" id="nombre" name="nombre" placeholder="Nombres" />
            </div>
            <div class="col-md-4">
              <input  type="text" class="form-control mb-3"  id="apellidos" name="apellidos" placeholder="Apellidos"/>
            </div>
          </div>
        </div>

        <div class="row justify-content-md-center mb-4">
          <h1>Escenario</h1>
        </div>

        <div class="row">

          <div class="col-md mb-3"></div>

          <div class="col-md-11 mb-3">
            <div class="row">

              @for($n = 0; $n < $aforo->columnas; $n++)

                <div class="col text-center">
                  <?php $s = $n+1; ?>
                  Col ({{$s}})
                </div>
              @endfor

            </div>
          </div>

        </div>

        <div class="row" id="calendario_resera">

          @for($i = 0; $i < $aforo->filas; $i++)

            <?php $f = $i+1; ?>

            <div class="col-md text-center">Fila ({{$f}})</div>

            <div class="col-md-11 mb-3">
              <div class="row">

                @for($n = 0; $n < $aforo->columnas; $n++)

                  <?php $s    = $n+1;
                  $silla      = $f.$s;
                  $color      = "";
                  $reservado  = "false";
                  $reservar   = "reservar";

                  if(!empty($disponibilidad)){
                    if(in_array($silla, $disponibilidad)){
                      $color      = 'background: #000';
                      $reservado  = 'true';
                      $reservar   = "";
                    }
                  } ?>

                  <div class="col {{$reservar}}" style="{{$color}}" data-codreserva=" {{$f}}{{$s}}" data-reservado="{{$reservado}}">
                    <img src="images/silla.png" style="width: 60%;"  class="img-fluid rounded mx-auto d-block" alt="Responsive image">
                    <div class="text-center"> ({{$f}} {{$s}}) </div>
                  </div>
                @endfor

              </div>
            </div>

          @endfor

        </div>

        <div class="btn btn-primary my-2" id="reservarbtn" >Enviar</div>
        {{ csrf_field() }}

      </form>

    </div>

  </body>

</html>
